set DIRNAME=%~dp0
set TargetDir = 
if "%DIRNAME%" == "" set DIRNAME=.

del /F /Q "%DIRNAME%\bin\debug\*.*"
del /F /Q "%DIRNAME%\bin\release\*.*"

xcopy /Y /R "%DIRNAME%\ListenerService\bin\debug\*.exe" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\debug\*.xml" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\debug\*.config" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\debug\*.dll" "%DIRNAME%\bin\debug"

xcopy /Y /R "%DIRNAME%\ListenerService\bin\release\*.exe" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\release\*.xml" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\release\*.config" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerService\bin\release\*.dll" "%DIRNAME%\bin\release"

xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\debug\*.exe" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\debug\*.xml" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\debug\*.config" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\debug\*.dll" "%DIRNAME%\bin\debug"

xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\release\*.exe" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\release\*.xml" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\release\*.config" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\ListenerServiceGui\bin\release\*.dll" "%DIRNAME%\bin\release"

xcopy /Y /R "%DIRNAME%\Client\bin\debug\*.exe" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\Client\bin\debug\*.xml" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\Client\bin\debug\*.config" "%DIRNAME%\bin\debug"
xcopy /Y /R "%DIRNAME%\Client\bin\debug\*.dll" "%DIRNAME%\bin\debug"

xcopy /Y /R "%DIRNAME%\Client\bin\release\*.exe" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\Client\bin\release\*.xml" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\Client\bin\release\*.config" "%DIRNAME%\bin\release"
xcopy /Y /R "%DIRNAME%\Client\bin\release\*.dll" "%DIRNAME%\bin\release"
