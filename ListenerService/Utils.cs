﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Security.Permissions;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace ListenerService
{
    public static class Utils
    {
        private static int DefaultPort = 24563;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string GetServiceName()
        {
            return ConfigurationManager.AppSettings["ServiceName"] ?? Properties.Resources.ServiceName;
        }

        public static bool LoggingIsEnabled()
        {
            return bool.Parse(ConfigurationManager.AppSettings["EnableLogging"]);
        }

        public static int GetPort()
        {
            try
            {
                string portString = ConfigurationManager.AppSettings["Port"] ?? $@"{DefaultPort}";

                int port = 0;

                if (!int.TryParse(portString, out port))
                {
                    port = DefaultPort;
                }

                return port;
            } catch (ConfigurationErrorsException e)
            {
                if (LoggingIsEnabled())
                {
                    logger.Error(e);
                }

                return DefaultPort;
            }
        }

        private static bool IsInstalled()
        {
            using (ServiceController controller = new ServiceController(GetServiceName()))
            {
                try
                {
                    ServiceControllerStatus status = controller.Status;
                } catch
                {
                    return false;
                }

                return true;
            }
        }

        private static bool IsRunning()
        {
            using (ServiceController controller = new ServiceController(GetServiceName()))
            {
                if (!IsInstalled())
                {
                    return false;
                }

                return (controller.Status == ServiceControllerStatus.Running);
            }
        }

        private static AssemblyInstaller GetInstaller()
        {
            AssemblyInstaller installer = new AssemblyInstaller(typeof(ListenerService).Assembly, null);

            installer.UseNewContext = true;

            return installer;
        }

        public static void InstallService()
        {
            if (IsInstalled())
            {
                return;
            }

            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();

                    try
                    {
                        installer.Install(state);
                        installer.Commit(state);
                    }
                    catch
                    {
                        try
                        {
                            installer.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                if (LoggingIsEnabled())
                {
                    logger.Error(e);
                }
                //throw;
            }
        }

        public static void UninstallService()
        {
            if (!IsInstalled())
            {
                return;
            }

            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();

                    try
                    {
                        installer.Uninstall(state);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                if (LoggingIsEnabled())
                {
                    logger.Error(e);
                }

                throw;
            }
        }

        public static void StartService()
        {
            if (!IsInstalled())
            {
                return;
            }

            using (ServiceController controller = new ServiceController(GetServiceName()))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Running)
                    {
                        controller.Start();
                        controller.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
                    }
                }
                catch (Exception e)
                {
                    if (LoggingIsEnabled())
                    {
                        logger.Error(e);
                    }

                    throw;
                }
            }
        }

        public static void StopService()
        {
            if (!IsInstalled())
            {
                return;
            }

            using (ServiceController controller = new ServiceController(GetServiceName()))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Stopped)
                    {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped,  TimeSpan.FromSeconds(10));
                    }
                }
                catch (Exception e)
                {
                    if (LoggingIsEnabled())
                    {
                        logger.Error(e);
                    }

                    throw;
                }
            }
        }
    }
}
