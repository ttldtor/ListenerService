﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using FTD2XX_NET;
using System.Runtime.InteropServices;

namespace ListenerService
{
    public partial class ListenerService : ServiceBase
    {
        private static readonly string ServerName = "xvcServer_v1.0:128\n";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static int? period;

        [DllImport("kernel32.dll")]
        private static extern IntPtr LoadLibrary(string dllToLoad);
        [DllImport("kernel32.dll")]
        private static extern bool FreeLibrary(IntPtr hModule);


        public ListenerService(string[] args)
        {
            InitializeComponent();
        }

        private static void LogInfo(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Info(text);
            }
        }

        private static void LogDebug(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Debug(text);
            }
        }

        private static void LogError(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Error(text);
            }
        }

        private static void LogError(Exception e)
        {
            logger.Error(e);
        }

        protected override async void OnStart(string[] args)
        {
            LogInfo("The Listener Service is started");

            var port = Utils.GetPort();

            LogInfo($"Port = {port}");

            var listener = new TcpListener(IPAddress.Any, port);

            listener.Start();

            while (true)
            {
                try
                {
                    var tcpClient = await listener.AcceptTcpClientAsync();
                    var t = Process(tcpClient);

                    await t;
                } catch (Exception e)
                {
                    LogError(e);
                }
            }
        }

        private async Task ProcessGetInfo(NetworkStream stream)
        {
            LogInfo($"Received command: 'getinfo'");

            byte[] response = Encoding.ASCII.GetBytes(ServerName);
            await stream.WriteAsync(response, 0, response.Length);

            LogInfo($"Replied with '{ServerName}'");
        }

        private static string ByteArrayToString(byte[] bytes, int index, int length)
        {
            StringBuilder hex = new StringBuilder(length);

            foreach( byte b in bytes.Skip(index).Take(length))
            {
                hex.AppendFormat("<{0:x2}>", b);
            }

            return hex.ToString();
        }

        private void ProcessShift(NetworkStream stream, byte[] readBuffer, int length)
        {
            LogInfo($"Received command: 'shift' [{length}]");

            LogInfo($"Replied with 'shift:{ByteArrayToString(readBuffer, 6, length - 6)}'");

            var numberOfBits = BitConverter.ToInt32(readBuffer, 6);

            LogInfo($"Number of bits: {numberOfBits}");
        }

        private async Task ProcessSetTck(NetworkStream stream, byte[] readBuffer, int length)
        {
            LogInfo($"Received command: 'settck' [{length}]");

            if (length > 11)
            {
                LogError($"'{ByteArrayToString(readBuffer, 7, length - 7)}' - is not an int32");

                return;
            }

            var newPeriod = BitConverter.ToInt32(readBuffer, 7);

            LogInfo($"New period: '{newPeriod}'");

            int oldPeriod;

            if (period == null)
            {
                oldPeriod = newPeriod;
            } else
            {
                oldPeriod = period.Value;
            }

            period = newPeriod;

            byte[] response = BitConverter.GetBytes(oldPeriod);

            await stream.WriteAsync(response, 0, response.Length);

            LogInfo($"Replied with '{oldPeriod}'");
        }

        private async Task Process(TcpClient client)
        {
            LogInfo($"Received connection request from {client.Client.RemoteEndPoint.ToString()}");

            try
            {
                var stream = client.GetStream();
                var readBuffer = new byte[client.ReceiveBufferSize];
                var length = await stream.ReadAsync(readBuffer, 0, client.ReceiveBufferSize);
                var needToWrite = false;

                if (length > 0)
                {
                    if (length >= 7)
                    {
                        var invalidCommand = true;
                        string cmd = Encoding.ASCII.GetString(readBuffer, 0, 2);


                        switch(cmd)
                        {
                            case "ge":
                                if (Encoding.ASCII.GetString(readBuffer, 0, 7).Equals("getinfo"))
                                {
                                    invalidCommand = false;
                                    await ProcessGetInfo(stream);
                                }

                                break;
                            case "sh":
                                if (Encoding.ASCII.GetString(readBuffer, 0, 6).Equals("shift:"))
                                {
                                    invalidCommand = false;
                                    ProcessShift(stream, readBuffer, length);
                                    needToWrite = true;
                                }

                                break;

                            case "se":
                                if (Encoding.ASCII.GetString(readBuffer, 0, 7).Equals("settck:"))
                                {
                                    invalidCommand = false;
                                    await ProcessSetTck(stream, readBuffer, length);
                                    needToWrite = true;
                                }

                                break;

                        }

                        if (invalidCommand)
                        {
                            LogError($"Invalid command {Encoding.UTF8.GetString(readBuffer)}");
                        }
                        else
                        if (needToWrite)
                        {
                            WriteToDevice(readBuffer);
                        }
                    }
                    else
                    {
                        LogError($"Invalid command {Encoding.UTF8.GetString(readBuffer)}");
                    }
                } else
                {
                    LogError("Buffer is empty");
                }

                client.Close();
                if (Utils.LoggingIsEnabled())
                {
                    logger.Info($"Connection is closed by server");
                }
            }
            catch (Exception e)
            {
                if (client.Connected)
                {
                    logger.Error(e);
                    client.Close();
                } else
                {
                    if (Utils.LoggingIsEnabled())
                    {
                        logger.Info($"Connection is closed by client");
                    }
                }
            }
        }

        protected override void OnStop()
        {
            LogInfo("The Listener Service is stopped");
        }

        private bool CheckFtdiLibrary()
        {
            var lib = LoadLibrary(@"FTD2XX.DLL");

            if (lib == IntPtr.Zero)
            {
                LogError($"Could not found ftd2xx.dll. Please install drivers or copy ftd2xx.dll library to the service directory or System32");

                return false;
            } else
            {
                FreeLibrary(lib);
            }
            
            return true;
        }

        private void WriteToDevice(byte[] request)
        {
            if (!CheckFtdiLibrary())
            {
                return;
            }

            LogInfo($"Writing to a device is started...");

            var deviceCount = 0u;
            FTDI.FT_STATUS status = FTDI.FT_STATUS.FT_OK;
            FTDI device = new FTDI();

            status = device.GetNumberOfDevices(ref deviceCount);

            if (status == FTDI.FT_STATUS.FT_OK)
            {
                LogInfo($"Number of FTDI devices: {deviceCount.ToString()}");
            }
            else
            {
                LogError($"Failed to get number of devices (error {status.ToString()})");

                return;
            }

            if (deviceCount == 0)
            {
                LogError($"Failed to get number of devices (error {status.ToString()})");

                return;
            }

            // Allocate storage for device info list
            var ftdiDeviceList = new FTDI.FT_DEVICE_INFO_NODE[deviceCount];

            // Populate our device list
            status = device.GetDeviceList(ftdiDeviceList);

            if (Utils.LoggingIsEnabled())
            {
                if (status == FTDI.FT_STATUS.FT_OK)
                {
                    for (uint i = 0; i < deviceCount; i++)
                    {
                        logger.Debug($"Device Index: {i}");
                        logger.Debug("Flags: " + String.Format("{0:x}", ftdiDeviceList[i].Flags));
                        logger.Debug("Type: " + ftdiDeviceList[i].Type.ToString());
                        logger.Debug("ID: " + String.Format("{0:x}", ftdiDeviceList[i].ID));
                        logger.Debug("Location ID: " + String.Format("{0:x}", ftdiDeviceList[i].LocId));
                        logger.Debug("Serial Number: " + ftdiDeviceList[i].SerialNumber.ToString());
                        logger.Debug("Description: " + ftdiDeviceList[i].Description.ToString());
                        logger.Debug("");
                    }
                }
            }


            // Open first device in our list by serial number
            status = device.OpenBySerialNumber(ftdiDeviceList[0].SerialNumber);
            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to open device (error {status.ToString()})");

                return;
            }

            // Set up device data parameters
            // Set Baud rate to 9600
            status = device.SetBaudRate(9600);
            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to set Baud rate (error {status.ToString()})");

                return;
            }

            // Set data characteristics - Data bits, Stop bits, Parity
            status = device.SetDataCharacteristics(FTDI.FT_DATA_BITS.FT_BITS_8, FTDI.FT_STOP_BITS.FT_STOP_BITS_1, FTDI.FT_PARITY.FT_PARITY_NONE);
            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to set data characteristics (error {status.ToString()})");

                return;
            }

            // Set flow control - set RTS/CTS flow control
            status = device.SetFlowControl(FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 0x11, 0x13);

            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to set flow control (error {status.ToString()})");

                return;
            }

            // Set read timeout to 5 seconds, write timeout to infinite
            status = device.SetTimeouts(5000, 0);

            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to set timeouts (error {status.ToString()})");

                return;
            }

            // Write string data to the device
            var data = request;
            var numBytesWritten = 0u;
            
            // Note that the Write method is overloaded, so can write string or byte array data
            status = device.Write(data, data.Length, ref numBytesWritten);

            if (status != FTDI.FT_STATUS.FT_OK)
            {
                LogError($"Failed to write to device (error {status.ToString()})");

                return;
            } else
            {
                LogInfo($"All data was successfully written");
            }

            device.Close();
            LogInfo($"Writing to a device is finished...");
        }
    }
}
