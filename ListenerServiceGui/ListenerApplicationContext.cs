﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ListenerServiceGui
{
    class ListenerApplicationContext: ApplicationContext
    {
        static readonly string loggerConfig = "NLog.config";
        static readonly string serviceExe = "ListenerService.exe";

        private NotifyIcon notifyIcon;
        private ConfigForm configForm;

        public ListenerApplicationContext()
        {
            configForm = new ConfigForm();

            notifyIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.app2,
                ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Install Service", (sender, args) => {
                        RunService("-install");
                    }),
                    new MenuItem("Uninstall Service", (sender, args) => {
                        RunService("-uninstall");
                    }),
                    new MenuItem("Start Service", (sender, args) => {
                        RunService("-start");
                    }),
                    new MenuItem("Stop Service", (sender, args) => {
                        RunService("-stop");
                    }),
                    new MenuItem("-"),
                    new MenuItem("Options...", (sender, args) => {
                        if (configForm.Visible)
                        {
                            return;
                        }

                        var config = LoadConfig();

                        configForm.Fill(config);

                        if (configForm.ShowDialog() == DialogResult.OK) {
                            SaveConfig(configForm.Collect());
                            RunService("-restart");
                        }
                    }),
                    new MenuItem("-"),
                    new MenuItem("Exit", (sender, args) => {
                        notifyIcon.Visible = false;

                        Application.Exit();
                    })
                }),
                Visible = true
            };
        }

        public void RunService(string arg)
        {
            ProcessStartInfo info = new ProcessStartInfo(@"ListenerService.exe");

            info.Arguments = arg;
            info.UseShellExecute = true;
            info.Verb = "runas";

            try
            {
                Process.Start(info);
            }
            catch
            {
                throw;
            }

        }

        public Config LoadConfig()
        {
            var result = new Config();
            var loggerConfigXml = new XmlDocument();

            try
            {
                loggerConfigXml.Load(loggerConfig);

                var targets = loggerConfigXml.GetElementsByTagName("target");

                if (targets.Count > 0)
                {
                    result.LogFile = targets[0].Attributes["fileName"].Value ?? "";
                }
            } catch
            {
                throw;
            }

            try
            {
                var serviceConfig = ConfigurationManager.OpenExeConfiguration(serviceExe);
                var serviceAppSettings = serviceConfig.AppSettings;

                string port = serviceAppSettings.Settings["Port"].Value;
                int portValue = 0;

                int.TryParse(port, out portValue);

                result.Port = portValue;

                string loggingIsEnabled = serviceAppSettings.Settings["EnableLogging"].Value;
                bool loggingIsEnabledValue = true;

                bool.TryParse(loggingIsEnabled, out loggingIsEnabledValue);

                result.LoggingIsEnabled = loggingIsEnabledValue;
            } catch
            {
                throw;
            }

            return result;
        }

        public void SaveConfig(Config config)
        {
            var loggerConfigXml = new XmlDocument();

            try
            {
                loggerConfigXml.Load(loggerConfig);

                var targets = loggerConfigXml.GetElementsByTagName("target");

                if (targets.Count > 0)
                {
                    targets[0].Attributes["fileName"].Value = config.LogFile;

                    loggerConfigXml.Save(loggerConfig);
                }
            }
            catch
            {
                throw;
            }

            try
            {
                var serviceConfig = ConfigurationManager.OpenExeConfiguration(serviceExe);
                var serviceAppSettings = serviceConfig.AppSettings;

                serviceAppSettings.Settings["Port"].Value = config.Port.ToString();
                serviceAppSettings.Settings["EnableLogging"].Value = config.LoggingIsEnabled.ToString();

                serviceConfig.Save();
            }
            catch
            {
                throw;
            }
        }
    }
}
