﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListenerServiceGui
{
    public class Config
    {
        public bool LoggingIsEnabled { get; set; }
        public string LogFile { get; set; }
        public int Port { get; set; }
    }
}
