﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListenerServiceGui
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();
        }

        public void Fill(Config config)
        {
            portText.Text = config.Port.ToString();
            enableLogCheckBox.Checked = config.LoggingIsEnabled;
            logFileTextBox.Text = config.LogFile;
        }

        public Config Collect()
        {
            return new Config()
            {
                Port = int.Parse(portText.Text),
                LoggingIsEnabled = enableLogCheckBox.Checked,
                LogFile = logFileTextBox.Text
            };
        }

        private void logFileTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void changeLogFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Filter = "log files (*.log)|*.log|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                logFileTextBox.Text = dialog.FileName;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The service will be restarted");
        }
    }
}
