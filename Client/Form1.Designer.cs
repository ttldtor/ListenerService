﻿namespace Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hostTextBox = new System.Windows.Forms.TextBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.getInfoButton = new System.Windows.Forms.Button();
            this.sendMessageGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SendMessageButton = new System.Windows.Forms.Button();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.testSendSetTckGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.sendSetTckButton = new System.Windows.Forms.Button();
            this.numberTextBox = new System.Windows.Forms.TextBox();
            this.numberLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.sendMessageGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.testSendSetTckGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.70423F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.29578F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.hostTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.portTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.getInfoButton, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.sendMessageGroupBox, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.testSendSetTckGroupBox, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(372, 281);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Host:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // hostTextBox
            // 
            this.hostTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hostTextBox.Location = new System.Drawing.Point(98, 3);
            this.hostTextBox.Name = "hostTextBox";
            this.hostTextBox.Size = new System.Drawing.Size(271, 20);
            this.hostTextBox.TabIndex = 4;
            this.hostTextBox.Text = "127.0.0.1";
            // 
            // portTextBox
            // 
            this.portTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portTextBox.Location = new System.Drawing.Point(98, 28);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(271, 20);
            this.portTextBox.TabIndex = 5;
            this.portTextBox.Text = "24563";
            // 
            // getInfoButton
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.getInfoButton, 2);
            this.getInfoButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.getInfoButton.Location = new System.Drawing.Point(3, 69);
            this.getInfoButton.Name = "getInfoButton";
            this.getInfoButton.Size = new System.Drawing.Size(366, 30);
            this.getInfoButton.TabIndex = 7;
            this.getInfoButton.Text = "Test \"getinfo\"";
            this.getInfoButton.UseVisualStyleBackColor = true;
            this.getInfoButton.Click += new System.EventHandler(this.getInfoButton_Click);
            // 
            // sendMessageGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.sendMessageGroupBox, 2);
            this.sendMessageGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.sendMessageGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendMessageGroupBox.Location = new System.Drawing.Point(3, 105);
            this.sendMessageGroupBox.Name = "sendMessageGroupBox";
            this.sendMessageGroupBox.Size = new System.Drawing.Size(366, 77);
            this.sendMessageGroupBox.TabIndex = 8;
            this.sendMessageGroupBox.TabStop = false;
            this.sendMessageGroupBox.Text = "Send message";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.72222F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.27778F));
            this.tableLayoutPanel2.Controls.Add(this.SendMessageButton, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.messageTextBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 41.37931F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 58.62069F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(360, 58);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // SendMessageButton
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.SendMessageButton, 2);
            this.SendMessageButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SendMessageButton.Location = new System.Drawing.Point(3, 27);
            this.SendMessageButton.MaximumSize = new System.Drawing.Size(0, 28);
            this.SendMessageButton.MinimumSize = new System.Drawing.Size(0, 28);
            this.SendMessageButton.Name = "SendMessageButton";
            this.SendMessageButton.Size = new System.Drawing.Size(354, 28);
            this.SendMessageButton.TabIndex = 8;
            this.SendMessageButton.Text = "Send";
            this.SendMessageButton.UseVisualStyleBackColor = true;
            this.SendMessageButton.Click += new System.EventHandler(this.SendMessageButton_Click);
            // 
            // messageTextBox
            // 
            this.messageTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageTextBox.Location = new System.Drawing.Point(91, 3);
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size(266, 20);
            this.messageTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Message:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // testSendSetTckGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.testSendSetTckGroupBox, 2);
            this.testSendSetTckGroupBox.Controls.Add(this.tableLayoutPanel3);
            this.testSendSetTckGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testSendSetTckGroupBox.Location = new System.Drawing.Point(3, 188);
            this.testSendSetTckGroupBox.Name = "testSendSetTckGroupBox";
            this.testSendSetTckGroupBox.Size = new System.Drawing.Size(366, 90);
            this.testSendSetTckGroupBox.TabIndex = 9;
            this.testSendSetTckGroupBox.TabStop = false;
            this.testSendSetTckGroupBox.Text = "Send \"settck\"";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.44444F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.55556F));
            this.tableLayoutPanel3.Controls.Add(this.sendSetTckButton, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.numberTextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.numberLabel, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.21127F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.78873F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(360, 71);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // sendSetTckButton
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.sendSetTckButton, 2);
            this.sendSetTckButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendSetTckButton.Location = new System.Drawing.Point(3, 28);
            this.sendSetTckButton.MaximumSize = new System.Drawing.Size(0, 28);
            this.sendSetTckButton.MinimumSize = new System.Drawing.Size(0, 28);
            this.sendSetTckButton.Name = "sendSetTckButton";
            this.sendSetTckButton.Size = new System.Drawing.Size(354, 28);
            this.sendSetTckButton.TabIndex = 9;
            this.sendSetTckButton.Text = "Send";
            this.sendSetTckButton.UseVisualStyleBackColor = true;
            this.sendSetTckButton.Click += new System.EventHandler(this.sendSetTckButton_Click);
            // 
            // numberTextBox
            // 
            this.numberTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numberTextBox.Location = new System.Drawing.Point(90, 3);
            this.numberTextBox.Name = "numberTextBox";
            this.numberTextBox.Size = new System.Drawing.Size(267, 20);
            this.numberTextBox.TabIndex = 8;
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numberLabel.Location = new System.Drawing.Point(3, 0);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(81, 25);
            this.numberLabel.TabIndex = 5;
            this.numberLabel.Text = "Number:";
            this.numberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 281);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Client";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.sendMessageGroupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.testSendSetTckGroupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox hostTextBox;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Button getInfoButton;
        private System.Windows.Forms.GroupBox sendMessageGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button SendMessageButton;
        private System.Windows.Forms.TextBox messageTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox testSendSetTckGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button sendSetTckButton;
        private System.Windows.Forms.TextBox numberTextBox;
        private System.Windows.Forms.Label numberLabel;
    }
}

