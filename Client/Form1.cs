﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class MainForm : Form
    {
        static readonly int DefaultPort = 24563;

        public MainForm()
        {
            InitializeComponent();
        }

        private int GetPort()
        {
            int port = DefaultPort;

            int.TryParse(portTextBox.Text, out port);

            return port;
        }

        private async Task SendTextAndShowResponse(string text)
        {
            try
            {
                var client = new TcpClient(hostTextBox.Text, GetPort());
                var stream = client.GetStream();


                byte[] data = Encoding.ASCII.GetBytes(text);

                var reader = new StreamReader(stream);
                await stream.WriteAsync(data, 0, data.Length);
                await stream.FlushAsync();

                byte[] buffer = new byte[client.ReceiveBufferSize];
                int count = await stream.ReadAsync(buffer, 0, client.ReceiveBufferSize);

                if (count > 0)
                {
                    MessageBox.Show($"{Encoding.ASCII.GetString(buffer)}", "Response");
                }

                stream.Close();
                client.Close();
            }
            catch
            {

            }
        }

        private async void SendMessageButton_Click(object sender, EventArgs e)
        {
            await SendTextAndShowResponse(messageTextBox.Text);
        }

        private int? GetNumber()
        {
            int result = 0;

            if (int.TryParse(numberTextBox.Text, out result))
            {
                return result;
            } else
            {
                MessageBox.Show($"'{numberTextBox.Text}' - not an int32");

                return null;
            }
        }

        private async void sendSetTckButton_Click(object sender, EventArgs e)
        {
            int? number = GetNumber();

            if (number == null)
            {
                return;
            }

            byte[] bytes = BitConverter.GetBytes(number.Value);

            try
            {
                var client = new TcpClient(hostTextBox.Text, GetPort());
                var stream = client.GetStream();
                var reader = new StreamReader(stream);

                byte[] data = Encoding.ASCII.GetBytes("settck:").Concat(bytes).ToArray();

                await stream.WriteAsync(data, 0, data.Length);
                await stream.FlushAsync();

                byte[] buffer = new byte[client.ReceiveBufferSize];

                int count = await stream.ReadAsync(buffer, 0, client.ReceiveBufferSize);

                if (count == 0)
                {
                    MessageBox.Show("The response is empty");

                    return;
                }

                int result = BitConverter.ToInt32(buffer, 0);

                MessageBox.Show($"{result}", "Response");

                stream.Close();
                client.Close();
            }
            catch
            {

            }
        }

        private async void getInfoButton_Click(object sender, EventArgs e)
        {
            await SendTextAndShowResponse("getinfo");
        }
    }
}
